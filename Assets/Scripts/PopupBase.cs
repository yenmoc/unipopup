﻿#pragma warning disable 0649
using UnityEngine;

// ReSharper disable CheckNamespace
namespace UnityModule.UniPopup
{
    public class PopupBase : MonoBehaviour, IPopupHandler
    {
        [SerializeField] private Canvas canvas;

        #region Implementation of IPopupHandler

        public Canvas Canvas => canvas;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void UpdateSortingOrder(int sortingOrder)
        {
            canvas.sortingOrder = sortingOrder;
        }

        #endregion
    }
}