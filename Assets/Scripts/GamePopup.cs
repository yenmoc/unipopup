﻿#pragma warning disable 0649
using UniRx;
using UnityEngine;

// ReSharper disable CheckNamespace
namespace UnityModule.UniPopup
{
    public partial class GamePopup : MonoBehaviour
    {
        private Popup _popup;
        [SerializeField] private Canvas canvas;

        /// <summary>
        /// initialize
        /// </summary>
        public void Initialized()
        {
            _popup = new Popup();
            _popup.SortingOrder.Subscribe(_ => canvas.sortingOrder = _);
        }
    }
}