# UniPopup

## Installation

```bash
"com.yenmoc.unipopup":"https://gitlab.com/yenmoc/unipopup"
or
npm publish --registry=http://localhost:4873
```

## Flow

### IPopupHandler
```csharp
    public interface IPopupHandler
    {
        /// <summary>
        /// canvas contains popup
        /// </summary>
        UnityEngine.Canvas Canvas { get; }

        /// <summary>
        /// active popup
        /// </summary>
        void Show();

        /// <summary>
        /// deactive popup
        /// </summary>
        void Hide();

        /// <summary>
        /// update sorting order of cavas contains popup
        /// </summary>
        /// <param name="sortingOrder"></param>
        void UpdateSortingOrder(int sortingOrder);
    }
```

### PopupBase
```csharp
	public class PopupBase : MonoBehaviour, IPopupHandler
    {
       //implement IPopupHandler
    }
```

### Popup
```csharp
    public class Popup
    {
        /// <summary>
        /// stack contains all popup (LIFO)
        /// </summary>
        private readonly Stack<IPopupHandler> _stacks = new Stack<IPopupHandler>();

        /// <summary>
        /// subjectproperty control sorting order of root canvas popup
        /// </summary>
        public SubjectProperty<int> SortingOrder { get; } = new SubjectProperty<int>();

        /// <summary>
        /// hide popup in top stack
        /// </summary>
        public void Hide(){}

        /// <summary>
        /// hide all popup in top stack
        /// </summary>
        public void HideAll(){}

        /// <summary>
        /// show popup
        /// </summary>
        /// <param name="popupHandler">popup wanna show</param>
        public void Show(IPopupHandler popupHandler){}

        /// <summary>
        /// show popup and hide previous popup
        /// </summary>
        /// <param name="popupHandler">popup wanna show</param>
        /// <param name="number">number previous popup wanna hide</param>
        public void Show(IPopupHandler popupHandler, int number){}

        /// <summary>
        /// show popup and hide all previous popup
        /// </summary>
        /// <param name="popupHandler">popup wanna show</param>
        public void ShowAndHideAll(IPopupHandler popupHandler){}
    }
```

### GamePopup
	-separate method of each popup write in here in partial method
```csharp
	public partial class GamePopup : MonoBehaviour
    {
        private Popup _popup;
        [SerializeField] private Canvas canvas;

        public void Initialized()
        {
            _popup = new Popup();
            _popup.SortingOrder.Subscribe(_ => canvas.sortingOrder = _);
        }
    }
```

